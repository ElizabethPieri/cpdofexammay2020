package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void testMinMax() throws Exception {

        int k= new MinMax().checkMethod(5,4);
        assertEquals("Add",5, k);

    }

    @Test
    public void testsecondMinMax() throws Exception {

        int k= new MinMax().checkMethod(5,10);
        assertEquals("Add",10, k);

    }

}

